# nodejs/typescript project archetype

  ### typescript
    Typescript configuration for nodejs
  ### ts-node-dev
    Typescript dev runner
  ### jest
    Jest testing tool configured with ts-jest transformer
  ### eslint
    Eslint configured to work with standard config
  ### husky
    Git precommit hooks that run the linter
